Write-Host "Copyright January 2018 Cedric Martinez"
Write-Host " This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>."

Write-Host "Hostname configuration"
$Server = Read-Host -Prompt 'Input your server name'
Write-Host "Setting hostname as $Server"
Rename-Computer -NewName $Server -PassThru -Force
Write-Host "Hostname set as requested"

Write-Host "Setting DNS Server"
$DNSIP = Read-Host -Prompt 'Input DNS IP'
$NICName = Read-Host -Prompt 'Input NIC Name'
netsh interface ip set dns $NICName static $DNSIP
Write-Host "DNS Server IP set to $DNSIP"
Write-Host "DNS configuration completed"

Write-Host "Setting Timezone"
Set-TimeZone -Name "W. Europe Standard Time"
Write-Host "Timezone set to W. Europe Standard Time"
Write-Host "Timezone configuration completed"

Write-Host "Stopping W32Time"
Stop-Service w32Time
Write-Host "W32Time stopped"
Write-Host "Setting NTP Server"
$NTPHOSTNAME = Read-Host -Prompt 'Input NTP Server FQDN'
w32tm /config /manualpeerlist:$NTPHOSTNAME /syncfromflags:MANUAL /reliable:yes /update
Write-Host "Set $NTPHOSTNAME as NTP Server"
Write-Host "Restarting W32Time"
Restart-Service w32Time
Write-Host "W32Time restarted"
Write-Host "Resyncing time"
w32tm /resync
Write-Host "Time synced"
Write-Host "Checking Time status"
w32tm /query /status
Write-Host "NTP configuration completed"
Write-Host "The Server will restart"
Restart-Computer
