#!/bin/bash
echo "Copyright December 2017 Cédric Martinez"
echo -e "Input desired FQDN: \c "
read Hostname
sudo hostname $Hostname
echo $Hostname
echo -e "Input Domain fqdn lowercase: \c "
read Domainfull
sudo echo "search $Domainfull" >> sudo /etc/resolv.conf
echo -e "Input DNS Server IP: \c "
read DNSServerIP
sudo echo "nameserver $DNSServerIP" >> sudo /etc/resolv.conf
echo "Installing needed packages: \c "
sudo yum install ntp sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation openldap-clients policycoreutils-python fail2ban yum-cron -y
echo -e "Input NTP Server Hostname: \c "
read NTPServerHostname
sudo echo "server $NTPServerHostname" >> sudo /etc/ntp.conf
echo "Stopping NTP Service \c "
sudo service ntpd stop
echo "NTP Service stopped \c "
echo "Syncronising Time with NTP Server \c "
sudo ntpdate -s $NTPServerHostname
echo "Time syncronised \c "
echo "Starting NTP service \c "
sudo service ntpd force-reload
echo "NTP service started \c "
echo "Configuring Timezone \c "
sudo mv /etc/localtime /etc/localtime.bak
ln -s /usr/share/zoneinfo/Europe/Zurich /etc/localtime
echo "Timezone set to Europe Zurich \c "
date
echo -e "Input Domain AdminUsername: \c "
read DomainAdminUsername
echo "Joining Domain \c "
sudo realm join --user=$DomainAdminUsername@$Domainfull $Domainfull
echo -e "Input Domain fqdn UPPERCASE: \c "
read DomainfullUppercase
echo -e "Input Domain NETBIOS UPPERCASE: \c "
read DomainUppercase
sudo authconfig --disablecache --enablewinbind --enablewinbindauth --smbsecurity=ads --smbworkgroup=$DomainUppercase --smbrealm=$DomainfullUppercase --enablewinbindusedefaultdomain --winbindtemplatehomedir=/home/actualit.info/%U --winbindtemplateshell=/bin/bash --enablekrb5 --krb5realm=$DomainfullUppercase --enablekrb5kdcdns --enablekrb5realmdns --enablelocauthorize --enablemkhomedir --enablepamaccess --updateall
sudo chkconfig oddjobd on && sudo chkconfig winbind on && sudo chkconfig messagebus on && sudo chkconfig realm on && sudo chkconfig smb on
sudo service oddjob restart && sudo service winbind restart && sudo service realm restart
kinit Administrator@$DomainfullUppercase
sudo net ads join $Domainfull -UAdministrator
sudo net ads testjoin
sudo service oddjob restart && sudo service winbind restart && sudo service realm restart
echo "Domain Joined \c "
sudo realm list
