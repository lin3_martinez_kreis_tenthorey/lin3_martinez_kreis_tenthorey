Write-Host "Copyright January 2018 Cedric Martinez"
Write-Host " This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>."

Write-Host "Importing modules"
Import-Module ServerManager
Write-Host "Powershell modules imported"

Write-Host "Installing ADDS Windows Role"
install-windowsfeature AD-Domain-Services
Write-Host "ADDS Windows Role installed"
Write-Host "Adding the ADDS Management Tools Windows Features"
Add-WindowsFeature RSAT-AD-PowerShell,RSAT-AD-AdminCenter,RSAT-ADDS-Tools
Write-Host "ADDS Management Tools Windows Features added"
Write-Host "Launch SetupDCPromote.ps1 to promote the server into a DC"

