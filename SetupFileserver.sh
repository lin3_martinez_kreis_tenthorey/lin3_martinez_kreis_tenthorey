#!/bin/bash
echo "Copyright December 2017 Cédric Martinez"
echo -e "Input desired FQDN: \c "
read Hostname
hostname $Hostname
echo -e "Input Domain fqdn lowercase: \c "
read Domainfull
sudo echo "search $Domainfull" >> sudo /etc/resolv.conf
echo -e "Input DNS Server IP: \c "
read DNSServerIP
echo "nameserver $DNSServerIP" >> sudo /etc/resolv.conf
echo "Installing needed packages: \c "
yum install ntp sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation openldap-clients policycoreutils-python fail2ban yum-cron -y
echo -e "Input NTP Server Hostname: \c "
read NTPServerHostname
echo "server $NTPServerHostname" >> sudo /etc/ntp.conf
echo "Stopping NTP Service \c "
service ntpd stop
echo "NTP Service stopped \c "
echo "Syncronising Time with NTP Server \c "
ntpdate -s $NTPServerHostname
echo "Time syncronised \c "
echo "Starting NTP service \c "
service ntpd start
echo "NTP service started \c "
echo "Configuring Timezone \c "
sudo mv /etc/localtime /etc/localtime.bak
ln -s /usr/share/zoneinfo/Europe/Zurich /etc/localtime
echo "Timezone set to Europe Zurich \c "
date
echo -e "Input Domain AdminUsername: \c "
read DomainAdminUsername
echo "Joining Domain \c "
sudo realm join --user=$DomainAdminUsername@$Domainfull $Domainfull
echo -e "Input Domain fqdn UPPERCASE: \c "
read DomainfullUppercase
echo -e "Input Domain NETBIOS UPPERCASE: \c "
read DomainUppercase
sudo authconfig --disablecache --enablewinbind --enablewinbindauth --smbsecurity=ads --smbworkgroup=$DomainUppercase --smbrealm=$DomainfullUppercase --enablewinbindusedefaultdomain --winbindtemplatehomedir=/home/actualit.info/%U --winbindtemplateshell=/bin/bash --enablekrb5 --krb5realm=$DomainfullUppercase --enablekrb5kdcdns --enablekrb5realmdns --enablelocauthorize --enablemkhomedir --enablepamaccess --updateall
sudo chkconfig oddjobd on && sudo chkconfig winbind on && sudo chkconfig messagebus on && sudo chkconfig realm on && sudo chkconfig smb on
sudo service oddjob restart && sudo service winbind restart && sudo service realm restart
kinit Administrator@$DomainfullUppercase
sudo net ads join $Domainfull -UAdministrator
sudo net ads testjoin
sudo service oddjob restart && sudo service winbind restart && sudo service realm restart
echo "Domain Joined \c "
sudo realm list
echo "Samba \c "
echo "Installing needed packages: \c "
yum install samba samba-common -y
echo "Creating base folder: \c "
mkdir /srv/smb
echo -e "Input folder name (lowercharonly): \c "
read SambaShareFolderName
echo "Creating share folder $SambaShareFolderName: \c "
mkdir /srv/smb/$SambaShareFolderName
echo "Configuring samba.conf: \c "
echo "[global]
workgroup = ACTUALIT
password server = $NTPServerHostname
realm = ACTUALIT.INFO
security = ads
idmap config * : range = 10000-20000
winbind separator = +
template homedir = /home/actualit.info/%U
template shell = /bin/bash
kerberos method = secrets only
winbind use default domain = true
winbind offline logon = false
server string = %h AWS
encrypt passwords = yes
printing = cups
dedicated keytab file = /etc/krb5.keytab
idmap config * : backend = tdb
idmap config * : range = 10000-20000
winbind enum users = Yes
winbind enum groups = Yes
winbind nested groups = Yes
winbind refresh tickets = yes
preferred master = no
dns proxy = no
wins server = $NTPServerHostname
wins proxy = no
inherit acls = Yes
map acl inherit = Yes
acl group control = yes
load printers = no
debug level = 3
use sendfile = no
printcap name = cups
load printers = yes
cups options = raw
winbind nested groups = Yes
server signing = auto

[$SambaShareFolderName]
comment = $SambaShareFolderName
path = /srv/smb/$SambaShareFolderName
read only = No
writeable = yes
browseable = yes
create mask = 0766
directory mask = 0755
valid users = @"Domain Users"" > /etc/smb/samba.conf
echo "Samba.conf configured: \c "
echo "Giving Domain Admins right to edit permissions: \c "
net rpc rights grant "ACTUALIT\Domain Admins" SeDiskOperatorPrivilege -U "ACTUALIT\administrator"
echo "Giving Domain Admins owner of samba share: \c "
chown root:"Domain Admins" /srv/smb/$SambaShareFolderName
echo "Defining linux file permissions on share folder: \c "
chmod 770 /srv/smb/$SambaShareFolderName*
echo "Restarting Samba Service: \c "
service smb restart
echo "Enabling Samba at startup: \c "
chkconfig samba on
